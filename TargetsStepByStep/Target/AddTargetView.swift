//
//  AddTargetView.swift
//  TargetsStepByStep
//
//  Created by Jamal on 3/9/20.
//  Copyright © 2020 Jamal. All rights reserved.
//

import SwiftUI

struct AddTargetView: View {
    @Environment(\.managedObjectContext) var moc
    @Environment(\.presentationMode) var presentaionMode
    
    @FetchRequest(
        entity: Target.entity(),
        sortDescriptors: [
            NSSortDescriptor(keyPath: \Target.order, ascending: false)
        ]
    ) var targets: FetchedResults<Target>
    
    @Binding var editTarget: Target?
    
    @State private var title: String = ""
    @State private var color: String = DefinedColors.colors[0]
    @State private var editMode = false
    
    
    var body: some View {
        List{
            AppTextField(label: "Type a title for your Dream:", text: $title).padding(.top)
            ColorsView(color: $color).padding(.vertical)
            
            HStack{
                Spacer()
                SubmitButton { self.submit()}

                if editMode{
                    DeleteButton{ self.delete() }
                }
                CancelButton{ self.getBack() }
                Spacer()
            }
        }
        .onAppear{
            if let target = self.editTarget{
                self.title = target.title!
                self.color = target.color!
                self.editMode = true
            }
        }
    }
    
    
    func submit(){
        editMode ? update(): add()
    }
    
    func add(){
        if title == "" {
            return
        }
        let target = Target(context: moc)
        target.title = title
        target.color = color
        target.id = UUID()

        target.order = targets.count>0 ? targets[0].order + 1: 0

        mocSave()
        
    }
    
    func update(){
        if title == "" {
            return
        }
        editTarget?.title = title
        editTarget?.color = color
        mocSave()
    }
    
    func delete(){
        if let target = editTarget{
            moc.delete(target)
            mocSave()
        }
    }
    
    func mocSave(){
        do {
            try moc.save()
            getBack()
        }catch{
            print("error on saving")
        }
    }
    
    func getBack(){
        presentaionMode.wrappedValue.dismiss()
        title = ""
        color = DefinedColors.colors[0]
    }
    
}
//struct AddTargetView_Previews: PreviewProvider {
//    static var previews: some View {
//        AddTargetView(editTarge: .constant(nil))
//    }
//}



