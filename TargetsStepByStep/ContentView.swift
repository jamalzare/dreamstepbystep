//
//  ContentView.swift
//  TargetsStepByStep
//
//  Created by Jamal on 3/8/20.
//  Copyright © 2020 Jamal. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        AppTabView()
    }
}

struct AppTabView: View {
    
    @State private var tabNum: Int = 0
    @State private var offset: CGFloat = -1000
    
    var body: some View {
        
        NavigationView{
            ZStack{
                Color.white.edgesIgnoringSafeArea(.all)
                
                VStack(spacing: 0){
                    ZStack{
                        PagerView(pageCount: 4, currentIndex: $tabNum){
                            //Color.white
                            
                            Tab1View()
                            Tab2View()
                            Tab3View()
                            Tab4View()
                        }
                        
                        SettingView().offset(x: offset)
                            .animation(.spring())
                        
                        VStack{
                            SettingButton(action: {
                                self.offset = self.offset == 0 ? -1000: 0
                            })
                            Spacer()
                        }
                    }
                    TabBar(tabNum: $tabNum)
                    
                }
                
                
            }
            .navigationBarTitle("")
            .navigationBarHidden(true)
        }
    }
}

struct TabBar : View {
    @Binding var tabNum: Int
    
    var body: some View {
        HStack{
            Spacer()
            TabItem(currentIndex: $tabNum, imageName: "moon.stars.fill", index: 0)
            TabItem(currentIndex: $tabNum, imageName: "pin.fill", index: 1)
            TabItem(currentIndex: $tabNum, imageName: "lightbulb.fill", index: 2)
            TabItem(currentIndex: $tabNum, imageName: "bell.fill", index: 3)
            Spacer()
        }
        .padding(.bottom, 4)
        .background(Color.white)
    }
}

struct TabItem : View {
    
    @Binding var currentIndex: Int
    
    var imageName: String
    var index: Int = 0
    
    var selected: Bool{
        return index == currentIndex
    }
    
    var body: some View {
        
        Button(action: {
            self.currentIndex = self.index
        }) {
            Image(systemName: imageName)
                .foregroundColor(Color.black.opacity(selected ? 0.5: 0.3))
                .padding()
                .background(selected ? Color.white: lightBlack)
                .background(Color.white)
                .font(Font.system(size: selected ? 25: 15))
                .clipShape(Circle())
                .shadow(radius: selected ? 1: 0)
                .padding(.horizontal, selected ? 25: 5)
        }
        .animation(.spring())
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct SettingButton: View {
    var action: () -> Void
    var body: some View {
        HStack{
            Spacer()
            Button(action: action){
                Image(systemName: "gear")
                    .padding(10)
                    .background(Color.white)
                    .foregroundColor(Color.black.opacity(0.5))
                    .font(Font.system(size: 25, weight: .heavy))
                    .clipShape(Circle())
                    .shadow(radius: 1)
            }
        }
    }
}
