//
//  AddStepTest.swift
//  TargetsStepByStep
//
//  Created by Jamal on 4/20/20.
//  Copyright © 2020 Jamal. All rights reserved.
//

import SwiftUI

struct AddStepTest: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello World!"/*@END_MENU_TOKEN@*/)
    }
}

struct AddStepTest_Previews: PreviewProvider {
    static var previews: some View {
        AddStepTest()
    }
}
