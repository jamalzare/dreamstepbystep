//
//  Bell.swift
//  TargetsStepByStep
//
//  Created by Jamal on 4/16/20.
//  Copyright © 2020 Jamal. All rights reserved.
//

import SwiftUI
import Firebase

struct Bell: Identifiable{
    var uid: UUID
    var id: String
    var title: String
    var text: String
    
    init(id: String, data: [String: Any]){
        uid = UUID()
        self.id = id
        title = data["title"] as! String
        text = data["text"] as! String
    }
}

struct Donate: Identifiable{
    var id = UUID()
    var buttonTitle: String
    var active: Bool = false
    var title: String = ""
    var text: String = ""
    
    init(buttonTitle: String) {
        self.buttonTitle = buttonTitle
    }
    
    init(data: [String: Any]) {
        buttonTitle = data["buttonTitle"] as! String
        active = data["active"] as! Bool
        buttonTitle = data["buttonTitle"] as! String
        title = data["title"] as! String
        let text = data["text"] as! String
        self.text = text.replacingOccurrences(of: "\\n", with: "\n")
        
    }
}

class BellVM: ObservableObject {
    
    @Published var bells: [Bell] = []
    @Published var donate: Donate = Donate(buttonTitle: "SuPpOrT uS")
    
    init(){
        
    }
    
    func getData(){
        
        self.getDonate()
        
        Firestore.firestore().collection("bells").getDocuments() {snapshot, error in
            if let error = error{
                print("error: \(error)")
            }else{
                
                if snapshot!.documents.count <= 0{
                    print("error")
                    //return
                }
                
                
                
                for document in snapshot!.documents{
                    let bell = Bell(id: document.documentID, data: document.data())
                    self.bells.append(bell)
                }
            }
        }
        
    }
    
    func getDonate(){
        
        Firestore.firestore().collection("donate").getDocuments() {snapshot, error in
            if let error = error{
                print("error: \(error)")
            }else{
                
                if snapshot!.documents.count <= 0{
                    print("error")
                    return
                }
                
                if let doc = snapshot?.documents[0]{
                    let data = doc.data()
                    self.donate = Donate(data: data)
                }
                
            }
        }
    }
}
