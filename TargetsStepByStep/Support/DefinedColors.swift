//
//  DefinedColors.swift
//  TargetsStepByStep
//
//  Created by Jamal on 3/9/20.
//  Copyright © 2020 Jamal. All rights reserved.
//

import Foundation
import SwiftUI

class DefinedColors {
    
    static let redMDString = "F44336"
    
    static let pinkMDString = "E91E63"
    
    static let purpleMDString = "9C27B0"
    
    static let deepPurpleMDString = "673AB7"
    
    static let indigoMDString = "3F51B5"
    
    static let blueMDString = "3F51B5"
    
    static let lightBlueMDString = "03A9F4"
    
    static let cyanMDString = "00BCD4"
    
    static let tealMDString = "009688"
    
    static let greenMDString = "4CAF50"
    
    static let lightGreenMDString = "8BC34A"
    
    static let limeMDString = "CDDC39"
    
    static let yellowMDString = "FFEB3B"
    
    static let amberMDString = "FFC107"
    
    static let orangeMDString = "FF9800"
    
    static let deepOrangeMDString = "FF5722"
    
    static let brownMDString = "795548"
    
    static let grayMDString = "9E9E9E"
    
    static let blueGrayMDString = "607D8B"
    
    
    public static let colors: [String] = {
        return[
            lightBlueMDString, pinkMDString, greenMDString, yellowMDString,
            tealMDString, orangeMDString, lightGreenMDString, redMDString,
            purpleMDString, amberMDString, cyanMDString, deepPurpleMDString,
            indigoMDString, grayMDString, limeMDString, blueMDString,
            blueGrayMDString, deepOrangeMDString, brownMDString,
        ]
    }()
}
