//
//  Tab1View.swift
//  TargetsStepByStep
//
//  Created by Jamal on 3/8/20.
//  Copyright © 2020 Jamal. All rights reserved.
//

import SwiftUI

extension Target: Identifiable{
    
}

struct Tab1View: View {
    
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(
        entity: Target.entity(),
        sortDescriptors: [
            NSSortDescriptor(keyPath: \Target.order, ascending: false)
        ]
    ) var targets: FetchedResults<Target>
    
    @State private var showAddView = false
    @State private var showDetailView = false
    @State private var selectedTarget: Target?
    
    @State private var editTarget: Target?
    
    private var helpText = "What Dreams Do you have? What goals do you want to achieve? what are your targets? Add Your dreams, goals, targets, desires here to work on it, TAP HERE or TAP On Plus Button (+) and in the form that will apear, type your dream title and choose a color just for beautify it and then submit it. For Edit a dream long press or long tab on that dream."
    
    var body: some View {
        VStack{
            titleView(title: "Dreams: \(targets.count)")
            
            if selectedTarget != nil {
                NavigationLink("", destination: TargetDetailView(target: selectedTarget!)
                                .environmentObject(selectedTarget!),
                                isActive: self.$showDetailView)
            }
            
            List{
                ForEach(targets, id:\.self) { target in
                    VStack{
                        TargetCard(target: target)
                            .id(target.title).id(target.color)
                            .onTapGesture { self.navigateToDetail(target) }
                            .onLongPressGesture { self.presentEditView(target)}
                    }
                }
                
                HelpView(text: helpText) {}
            }
            
            AddButton{ self.presentAddView() }
        }
        .background(Color.white)
            
        .sheet(isPresented: $showAddView){
            AddTargetView(editTarget: self.$editTarget)
                .environment(\.managedObjectContext, self.moc)
        }
        
    }
    
    func navigateToDetail(_ target: Target){
        selectedTarget = target
        showDetailView = true
    }
    
    func presentEditView(_ target: Target){
        editTarget = target
        showAddView = true
    }
    
    func presentAddView(){
        editTarget = nil
        showAddView = true
    }
    
}

struct Tab1Content_Previews: PreviewProvider {
    static var previews: some View {
        Tab1View()
    }
}


struct TargetCard: View {
    @State var target: Target
    @EnvironmentObject var setting: AppSetting
    
    var body: some View {
        Text("\(target.title!)")
            .fontWeight(.heavy)
            .foregroundColor(Color.black.opacity(0.5))
            .font(Font.system(size: setting.fontSize))
            .lineLimit(1)
            .padding(12)
            .background(Color(hexString: target.color!).opacity(0.5))
            .clipShape(Capsule())
            .frame(maxWidth: .infinity, minHeight:100, alignment: .center)
    }
}
