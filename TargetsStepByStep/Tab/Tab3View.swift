//
//  Tab3View.swift
//  TargetsStepByStep
//
//  Created by Jamal on 3/19/20.
//  Copyright © 2020 Jamal. All rights reserved.
//

import SwiftUI

struct Tab3View: View {
    
    private var guides = Guide.guides
    
    @State private var showAddView = false
    
    var body: some View {
        
        VStack{
            titleView(title: "Guidance")
            
            List{
                ForEach(guides){ guide in
                    TitleCardView(title: guide.title, text: guide.content[0].text, color: lightBlack)
                }
            }
            AppJustForSpacing()
        }
    }
}


struct Tab3View_Previews: PreviewProvider {
    static var previews: some View {
        Tab3View()
    }
}
