//
//  Tab4View.swift
//  TargetsStepByStep
//
//  Created by Jamal on 4/15/20.
//  Copyright © 2020 Jamal. All rights reserved.
//

import SwiftUI
import Firebase

/*
 tasks
 1- check if bells not fetched try again on gete this tab
 2- errors for fetching bells
 3- lineBreak for text
 4- set permission for Firestore database write/delete
 5- refressh bells for load new ones
*/
struct Tab4View: View {
    
    @ObservedObject var bellVM = BellVM()
    @State var showDonation: Bool = false
    @State private var notShowAnyThing = false
    
    private var helpText = "In this Tab from time to time we will send you Contents that will help you to achieve your goals, to find your ways, better understanding the how to act and tips, points, guidances that will help you and guide you. to receive these contents, you need to connected to internet on your device."
    
    var body: some View {
        VStack{
            Header(title: "Bells: \(bellVM.bells.count)",
                donate: $bellVM.donate,
                presentMode: $showDonation)
            
            List{
                ForEach(bellVM.bells, id:\.uid){ bell in
                    TitleCardView(title: bell.title, text: bell.text, color: .white)
                }
                HelpView(text: helpText) {}
                
            }
            
            RefreshButton {
                self.bellVM.getData()
            }
        }
        .onAppear{
            self.bellVM.getData()
        }
            
        .sheet(isPresented: $showDonation){
            DonateView(donate: self.bellVM.donate)
        }
    }
    
    
    func addData(){
        
        let data : [String: Any] = [
            "title": "4 title",
            "text": "this 4 text for test and its awsome"
        ]
        let docRef = Firestore.firestore().document("bells/\(UUID().uuidString)")
        docRef.setData(data){ error in
            if let error = error{
                print("error occured: \(error)")
            }else{
                print("data Saved successfully")
            }
        }
    }
}

struct DonateView: View {
    var donate: Donate
    
    var body: some View {
        VStack{
            List{
                TitleCardView(title: donate.title, text: donate.text, color: lightBlack)
            }
        }
        .padding(.top)
    }
    
}
