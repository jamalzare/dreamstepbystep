//
//  Tab2View.swift
//  TargetsStepByStep
//
//  Created by Jamal on 3/14/20.
//  Copyright © 2020 Jamal. All rights reserved.
//

import SwiftUI

struct Tab2View: View {
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(
        entity: Tips.entity(),
        sortDescriptors: [
             NSSortDescriptor(keyPath: \Tips.order, ascending: false)
        ]
        
    ) var tips: FetchedResults<Tips>
    
    @State private var showAddView = false
    @State private var selectedTip: Tips?
    
    private var helpText = "What did you understand or learn today? What do you need review daily? Add Here Tips, Understandings, Points, Notes for Daily review, to Add Tips TAP HERE or TAP On Plus Button (+) and in the form that will Apear type the tips that you want to review daily and select a color for beautify it then submit it."
    
    var body: some View {
        VStack{
            titleView(title: "Tips: \(tips.count) ")
            
            List{
                
                ForEach(tips, id:\.self){ tip in
                    Button(action: {
                        self.selectedTip = tip
                        self.showAddView.toggle()
                    }){
                        CardView(text: "\(tip.text!) order:\(tip.order) pin:\(tip.isPined ? "true": "false")", color: Color(hexString: tip.color!).opacity(0.15))
                    }
                }
                HelpView(text: helpText){}
                
            }
            AddButton{
                self.selectedTip = nil
                self.showAddView = true
            }
        }
        .sheet(isPresented: $showAddView){
            AddTipsView(tip: self.selectedTip)
                .environment(\.managedObjectContext, self.moc)
        }
        
    }
    
    
}

struct Tab2View_Previews: PreviewProvider {
    static var previews: some View {
        Tab2View()
    }
}


