//
//  SwiftUIView.swift
//  TargetsStepByStep
//
//  Created by Jamal on 5/20/20.
//  Copyright © 2020 Jamal. All rights reserved.
//

import SwiftUI

//back up tab1



struct StepsListBK: View {
    @Environment(\.managedObjectContext) var moc
    @EnvironmentObject var target: Target
    @Binding var stepsCount: Int
    @State private var steps: [Step] = []
    @State private var showAddView: Bool = false
    @State private var editStep: Step?
    
    private let helpText = "What Actions do you think that will help you to achieve this dream? What steps you can do for this dream come true? Do whatever you think and feel good about it. Submit here the steps that you take, To do this TAP HERE or Tap on the Plus Button (+) and fill the form that will appear."
    
    var body: some View {
        
        VStack{
            List{
                ForEach(self.target.steps?.allObjects as? [Step] ?? [Step](), id:\.self) { step in
                    VStack{
                        StepCardViewBK(step: step)
                            .onTapGesture {
                                self.editStep = step
                                self.showAddView = true
                        }
                    }
                }
                
                HelpView(text: helpText) {}
            }
            
            AddButton{
                self.editStep = nil// Step(context: self.moc)
                self.showAddView = true
            }
        }
        .background(Color.white)
            
        .sheet(isPresented: $showAddView){
            AddStepView(step: self.$editStep).environmentObject(self.target)
                .environment(\.managedObjectContext, self.moc)
        }
    }
    
    func showDetail(_ step: Step){
        
    }
}

struct StepCardViewBK: View {
    
    @State var step: Step
    var body: some View {
        HStack{
            
            Text(step.title!)
                .fontWeight(.heavy)
                .font(Font.system(size: 18))
                .lineLimit(10)
                .foregroundColor(Color.black.opacity(0.6))
                .padding()
                .background(Color(hexString: step.color!).opacity(0.5))
                .cornerRadius(40)
                .padding(.vertical, 7)
            Spacer()
        }
        
        
    }
}

struct Tab1Content_PreviewsBK: PreviewProvider {
    static var previews: some View {
        Tab1View()
    }
}

